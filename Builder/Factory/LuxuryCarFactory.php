<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 04.05.15
 * Time: 13:01
 */

namespace Builder\Factory;


class LuxuryCarFactory extends FactoryBase
{
    public function construct()
    {
        $this->_builder->buildFrame();
        $this->_builder->buildEngine();
        $this->_builder->buildWheels();
        $this->_builder->buildSafety();
        $this->_builder->buildMultimedia();
        $this->_builder->buildLuxury();
        return $this->_builder->getCar();
    }

} 