<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 04.05.15
 * Time: 12:57
 */

namespace Builder\Factory;


class CheapCarFactory extends FactoryBase
{
    public function construct()
    {
        $this->_builder->buildFrame();
        $this->_builder->buildEngine();
        $this->_builder->buildWheels();
        $this->_builder->buildSafety();
        return $this->_builder->getCar();
    }
} 