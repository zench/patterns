<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 04.05.15
 * Time: 12:39
 */

namespace Builder\Factory;


use Builder\Builder\BuilderBase;

abstract class FactoryBase
{
    protected $_builder;

    public function __construct(BuilderBase $builder)
    {
        $this->_builder = $builder;
    }

    abstract public function construct();
} 