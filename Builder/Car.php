<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 04.05.15
 * Time: 12:34
 */

namespace Builder;


class Car
{
    public $frame;
    public $wheels;
    public $engine;
    public $luxury;
    public $multimedia;
    public $safety;

    public function __toString()
    {
        return "Frame: ".$this->frame."<br />".
        "Engine: ".$this->engine."<br />".
        "Wheels: ".$this->wheels."<br />".
        "Safety: ".$this->safety."<br />".
        "Multimedia: ".$this->multimedia."<br />".
        "Luxury: ".$this->luxury."<br />";
    }
} 