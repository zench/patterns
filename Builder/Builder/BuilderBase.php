<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 04.05.15
 * Time: 12:42
 */

namespace Builder\Builder;


use Builder\Car;

abstract class BuilderBase
{
    protected $car;

    public function __construct()
    {
        $this->car = new Car();
    }

    public function getCar()
    {
        return $this->car;
    }

    abstract public function buildFrame();
    abstract public function buildEngine();
    abstract public function buildWheels();
    abstract public function buildSafety();
    abstract public function buildMultimedia();
    abstract public function buildLuxury();
} 