<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 04.05.15
 * Time: 12:46
 */

namespace Builder\Builder;


class VolksWagenBuilder extends BuilderBase
{

    public function buildFrame()
    {
        $this->car->frame = "VW frame";
    }

    public function buildEngine()
    {
        $this->car->engine = "VW engine 1.6 TSI";
    }

    public function buildWheels()
    {
        $this->car->wheels = "Michelin wheels 16 inches";
    }

    public function buildSafety()
    {
        $this->car->safety  = "4 air bags";
    }

    public function buildMultimedia()
    {
        $this->car->multimedia  = "Pioneer multimedia";
    }

    public function buildLuxury()
    {
        $this->car->luxury  = "VW luxury package";
    }
}