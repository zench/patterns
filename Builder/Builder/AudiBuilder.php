<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 04.05.15
 * Time: 12:54
 */

namespace Builder\Builder;


class AudiBuilder extends BuilderBase
{

    public function buildFrame()
{
    $this->car->frame = "Audi frame";
}

    public function buildEngine()
{
    $this->car->engine = "Audi engine 2.4 TSI";
}

    public function buildWheels()
{
    $this->car->wheels = "Michelin wheels 17 inches";
}

    public function buildSafety()
{
    $this->car->safety  = "8 air bags";
}

    public function buildMultimedia()
{
    $this->car->multimedia  = "Sony multimedia";
}

    public function buildLuxury()
{
    $this->car->luxury  = "Audi luxury package";
}

} 