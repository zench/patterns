<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 29.04.15
 * Time: 23:13
 */

namespace State\States;


use State\Car;

class EmptyTankState implements StateInterface
{
    private $_car;

    public function __construct(Car $car)
    {
        $this->_car = $car;
    }

    public function fillTank()
    {
        echo "Бак наполнен, можно ехать<br />";
        $this->_car->increaseGasoline(70);
        $this->_car->setState($this->_car->fullTank);
    }

    public function turnKey()
    {
        echo "Сначала наполни бак, потом поворачивай ключ<br />";
    }

    public function drive()
    {
        echo "Без бензина не поеду<br />";
    }

    public function stop()
    {
        echo "И так стою<br />";
    }
}