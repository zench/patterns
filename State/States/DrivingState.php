<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 29.04.15
 * Time: 23:14
 */

namespace State\States;


use State\Car;

class DrivingState implements StateInterface
{
    private $_car;

    public function __construct(Car $car)
    {
        $this->_car = $car;
    }

    public function fillTank()
    {
        echo "Отлично, дозаправка в воздухе, я настоящий штурмовик!<br />";
    }

    public function turnKey()
    {
        echo "Убери руки от ключа<br />";
    }

    public function drive()
    {
        echo "Мы и так едем<br />";
        $this->_car->increaseGasoline(-30);
    }

    public function stop()
    {
        echo "Тормозим<br />";
        $this->_car->setState($this->_car->engineStarted);
    }
}