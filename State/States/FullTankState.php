<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 29.04.15
 * Time: 23:13
 */

namespace State\States;


use State\Car;

class FullTankState implements StateInterface
{
    private $_car;

    public function __construct(Car $car)
    {
        $this->_car = $car;
    }

    public function fillTank()
    {
        echo "В меня столько не влезет<br />";
    }

    public function turnKey()
    {
        echo "Дрынь-дрынь-дрынь-дрынь<br />";
        $this->_car->setState($this->_car->engineStarted);
    }

    public function drive()
    {
        echo "Сначала поверни ключ<br />";
    }

    public function stop()
    {
        echo "Стою, стою<br />";
    }
}