<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 29.04.15
 * Time: 23:15
 */

namespace State\States;


use State\Car;

class EngineStartedState implements StateInterface
{
    private $_car;

    public function __construct(Car $car)
    {
        $this->_car = $car;
    }

    public function fillTank()
    {
        echo "Определись, ты хочешь ехать или наполнить бак<br />";
    }

    public function turnKey()
    {
        echo "Уже завелся<br />";
    }

    public function drive()
    {
        echo "В путь, ура!<br />";
        $this->_car->setState($this->_car->driving);
    }

    public function stop()
    {
        echo "Еще даже не начинали ехать!<br />";
    }
}