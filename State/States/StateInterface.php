<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 29.04.15
 * Time: 23:09
 */

namespace State\States;


interface StateInterface
{
    public function fillTank();
    public function turnKey();
    public function drive();
    public function stop();
} 