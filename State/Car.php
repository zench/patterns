<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 29.04.15
 * Time: 23:09
 */

namespace State;


use State\States\DrivingState;
use State\States\EmptyTankState;
use State\States\EngineStartedState;
use State\States\FullTankState;
use State\States\StateInterface;

class Car
{
    private $_gasoline = 0;
    private $_tankSize = 70;

    public $emptyTank;
    public $fullTank;
    public $driving;
    public $engineStarted;
    public $currentState;

    public function __construct()
    {
        $this->driving = new DrivingState($this);
        $this->emptyTank = new EmptyTankState($this);
        $this->fullTank = new FullTankState($this);
        $this->engineStarted = new EngineStartedState($this);
        $this->currentState = $this->emptyTank;
    }

    public function fillTank()
    {
        echo "Наполняем бак: ";
        $this->currentState->fillTank();
    }

    public function turnKey()
    {
        echo "Поворачиваем ключ: ";
        $this->currentState->turnKey();
    }

    public function drive()
    {
        echo "Пытаемся ехать: ";
        $this->currentState->drive();
    }

    public function stop()
    {
        echo "Останавливаемся: ";
        $this->currentState->stop();
    }

    public function setState(StateInterface $state)
    {
        $this->currentState = $state;
    }

    public function increaseGasoline($count)
    {
        $this->_gasoline = ($this->_gasoline + $count)%$this->_tankSize;
    }
} 