<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 10.03.2015
 * Time: 23:26
 */

namespace Decorator;


class GreenTea extends BeverageBase
{
    protected $_cost = 120;
    protected $_description = 'This is green leaf Tea';

    public function getCost()
    {
        return $this->_cost;
    }

    public function getDescription()
    {
       return $this->_description;
    }
}