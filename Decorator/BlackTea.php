<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 10.03.2015
 * Time: 23:23
 */

namespace Decorator;


class BlackTea extends BeverageBase
{
    protected $_cost = 100;
    protected $_description = 'This is black Tea';

    public function getCost()
    {
        return $this->_cost;
    }

    public function getDescription()
    {
        return $this->_description;
    }
}