<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 10.03.2015
 * Time: 23:17
 */

namespace Decorator;


abstract class BeverageBase
{
    protected $_cost;
    protected $_description;

    public abstract function getCost();

    public abstract function getDescription();

    public function getSummary()
    {
        echo 'Description: '.$this->getDescription()."<br />Cost: ".$this->getCost()."<br /><br />";
    }
}