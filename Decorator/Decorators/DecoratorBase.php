<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 10.03.2015
 * Time: 23:20
 */

namespace Decorator\Decorators;


use Decorator\BeverageBase;

abstract class DecoratorBase extends BeverageBase
{
    protected $_beverage;

    protected $_description;

    protected $_cost;

    public function __construct(BeverageBase $_beverage)
    {
        $this->_beverage = $_beverage;
    }

    public function getCost()
    {

    }

    public function getDescription()
    {

    }
}