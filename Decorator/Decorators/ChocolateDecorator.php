<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 10.03.2015
 * Time: 23:31
 */

namespace Decorator\Decorators;


class ChocolateDecorator extends DecoratorBase
{
    protected $_cost = 50;

    protected $_description = 'Chocolate (portion)';

    public function getCost()
    {
        return $this->_beverage->getCost() + $this->_cost;
    }

    public function getDescription()
    {
        return $this->_beverage->getDescription().' + '.$this->_description;
    }
}