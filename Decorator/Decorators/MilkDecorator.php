<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 10.03.2015
 * Time: 23:30
 */

namespace Decorator\Decorators;


class MilkDecorator extends DecoratorBase
{
    protected $_cost = 40;

    protected $_description = 'Milk (portion)';

    public function getCost()
    {
        return $this->_beverage->getCost() + $this->_cost;
    }

    public function getDescription()
    {
        return $this->_beverage->getDescription().' + '.$this->_description;
    }
}