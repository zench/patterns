<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 10.03.2015
 * Time: 23:27
 */

namespace Decorator\Decorators;


class SugarDecorator extends DecoratorBase
{
    protected $_cost = 20;

    protected $_description = 'Sugar (portion)';

    public function getCost()
    {
        return $this->_beverage->getCost() + $this->_cost;
    }

    public function getDescription()
    {
        return $this->_beverage->getDescription().' + '.$this->_description;
    }
}