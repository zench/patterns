<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 10.03.2015
 * Time: 23:26
 */

namespace Decorator;


class Espresso extends BeverageBase
{
    protected $_cost = 150;
    protected $_description = 'This is classic Espresso';

    public function getCost()
    {
       return $this->_cost;
    }

    public function getDescription()
    {
        return $this->_description;
    }
}