<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 12:28
 */

namespace Application;


use Adapter\CatInterfaceAdapter;
use Adapter\CatPrinter;
use Adapter\HomeCat;
use Adapter\StripedCat;
use Adapter\Tiger;
use Builder\Builder\AudiBuilder;
use Builder\Builder\VolksWagenBuilder;
use Builder\Factory\CheapCarFactory;
use Builder\Factory\LuxuryCarFactory;
use Command\Commands\LightCommand;
use Command\Commands\MusicCommand;
use Command\Commands\TeapotCommand;
use Command\Commands\TvCommand;
use Command\ControlledSystems\Light;
use Command\ControlledSystems\Music;
use Command\ControlledSystems\Teapot;
use Command\ControlledSystems\Tv;
use Command\RemoteControl;
use Decorator\BeverageBase;
use Decorator\BlackTea;
use Decorator\Decorators\MilkDecorator;
use Decorator\Decorators\SugarDecorator;
use Decorator\Espresso;
use Ducks\CommonDuck;
use Ducks\Flyable\CommonFlyable;
use Ducks\Flyable\SpecialFlyable;
use Ducks\Quakable\CommonQuakable;
use Ducks\Quakable\SpecialQuakable;
use Ducks\SpecialDuck;
use AbstractFactory\Facilities\DeutschVolkswagenFacility;
use AbstractFactory\Facilities\RussianVolkswagenFacility;
use Facade\Dryer;
use Facade\Engine;
use Facade\Thermo;
use Facade\WashingMachine;
use Facade\WaterManagementSubsystem;
use Observer\Observers\AndroidObserver;
use Observer\Observers\IOSObserver;
use Observer\Subjects\TraktirSubject;
use Observer\Storages\TraktirStorage;
use State\Car;
use TemplateMethod\Hamburger;
use TemplateMethod\HotDog;

class Application
{
    public static function start()
    {
        //1-st lesson
        /*$ducks = array(new CommonDuck(new CommonFlyable(), new CommonQuakable()),
                        new SpecialDuck(new SpecialFlyable(), new SpecialQuakable()));
        foreach($ducks as $duck) {
            $duck->display();
            echo "<br />";
            $duck->swim();
            echo "<br />";
            $duck->fly();
            echo "<br />";
            $duck->quak();
            echo "<br />";
        }*/

        //2-nd lesson
        /*
        $traktirSubject = new TraktirSubject();
        $traktirStorage = new TraktirStorage('Меню', 'Заказы', 'Сотрудники');
        $traktirSubject->setStorage($traktirStorage);

        $androidObserver = new AndroidObserver($traktirSubject);
        $iosObserver = new IOSObserver($traktirSubject);
        $traktirSubject->notifyObservers();
        */

        //3-rd lesson
        /*
        $blackTeaWithMilkAndSugar = new SugarDecorator(new SugarDecorator(new MilkDecorator(new BlackTea())));
        $capuchino = new SugarDecorator(new MilkDecorator(new Espresso()));
        $blackTeaWithMilkAndSugar->getSummary();
        $capuchino->getSummary();
        */

        //4-th lesson part 1 - fabric method
        /*$facility = new DeutschVolkswagenFacility();
        $facility->getCar('Golf');
        $facility->getCar('Passat');
        $facility->getCar('Tiguan');
        $facility->getCar('Touareg');*/

        //4-th lesson part 2 - abstract factory
        /*
        $facility = new RussianVolkswagenFacility();
        $facility->getCar('Golf');
        $facility->getCar('Passat');
        $facility->getCar('Tiguan');
        $facility->getCar('Touareg');*/

        //5-th lesson Command pattern
        /*Header("Content-Type: text/html;charset=UTF-8");
        $form = '<form action = "/" method = "post"><input type = "text" name = "command"><input type = "submit" value = "нажать" name = "submit"></form>';
        $remote = new RemoteControl();
        $remote->setButtonForCommand(1, new LightCommand(new Light()));
        $remote->setButtonForCommand(2, new TvCommand(new Tv()));
        $remote->setButtonForCommand(3, new MusicCommand(new Music()));
        $remote->setButtonForCommand(4, new TeapotCommand(new Teapot()));

        echo (string)$remote;
        echo $form;
        if(isset($_POST['command'])) {
            $remote->pushButton($_POST['command']);
            $remote->pushButton($_POST['command']);
            $remote->pushButton($_POST['command']);
            $remote->pushButton($_POST['command']);
            $remote->undoButton($_POST['command']);
            $remote->undoButton($_POST['command']);
            $remote->undoButton($_POST['command']);
            $remote->pushButton($_POST['command']);
            $remote->pushButton($_POST['command']);
        }*/

        //6-th lesson Adapter pattern
        /*$strippy = new StripedCat('Strippy');
        $tiger = new Tiger();
        $catInterfaceAdapter = new CatInterfaceAdapter($tiger);
        $catPrinter = new CatPrinter($catInterfaceAdapter);
        $catPrinter->printCat();*/

        //7-th lesson Facade pattern
        /*$washingMachine = new WashingMachine(new Engine(), new Thermo(), new WaterManagementSubsystem(), new Dryer());
        echo "Cotton washing<br />";
        $washingMachine->cottonWashing();
        echo "<br /><br />";
        echo "Wool washing<br />";
        $washingMachine->woolWashing();
        echo "<br /><br />";
        */

        //8-th lesson TemplateMethod
        /*$hamburger = new Hamburger();
        $hotdog = new HotDog();
        $hotdog->customerWantsTopping();
        echo "Hamburger:<br />";
        $hamburger->prepare();
        echo "<br /><br />";
        echo "HotDog:<br />";
        $hotdog->prepare();*/

        //9-th lesson State
        /*Header("Content-Type: text/html;charset=UTF-8");
        $car = new Car();
        $car->drive();
        $car->fillTank();
        $car->drive();
        $car->turnKey();
        $car->drive();
        $car->drive();
        $car->stop();
        $car->fillTank();*/

        //10-th lesson Builder
        echo "Cheap VolksWagen:<br />";
        $vwBuilder = new VolksWagenBuilder();
        $cheapCarFactory = new CheapCarFactory($vwBuilder);
        $cheapVW = $cheapCarFactory->construct();
        echo $cheapVW;
        echo "<br />Luxury VolksWagen:<br />";
        $vwBuilder = new VolksWagenBuilder();
        $luxuryCarFactory = new LuxuryCarFactory($vwBuilder);
        $luxuryVW = $luxuryCarFactory->construct();
        echo $luxuryVW;

        echo "<br />Cheap Audi:<br />";
        $audiBuilder = new AudiBuilder();
        $cheapCarFactory = new CheapCarFactory($audiBuilder);
        $cheapAudi = $cheapCarFactory->construct();
        echo $cheapAudi;

        echo "<br />Luxury Audi:<br />";
        $audiBuilder = new AudiBuilder();
        $luxuryCarFactory = new LuxuryCarFactory($audiBuilder);
        $luxuryAudi = $luxuryCarFactory->construct();
        echo $luxuryAudi;

    }

}