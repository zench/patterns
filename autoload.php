<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 12:40
 */

function __autoload($className)
{
    $file = $_SERVER["DOCUMENT_ROOT"].'/'.$className.'.php';
    if(file_exists($file) != false) {
        include_once($file);
    }
}