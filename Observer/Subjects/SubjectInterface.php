<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 05.03.2015
 * Time: 22:10
 */

namespace Observer\Subjects;


use Observer\Observers\ObserverInterface;

interface SubjectInterface
{
    public function registerObserver(ObserverInterface $observer);

    public function removeObserver(ObserverInterface $observer);

    public function notifyObservers();
}