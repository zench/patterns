<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 05.03.2015
 * Time: 22:20
 */

namespace Observer\Subjects;


use Observer\Observers\ObserverInterface;
use Observer\Storages\StorageInterface;

class TraktirSubject implements SubjectInterface
{
    private $_observers = array();
    private $_storage;

    public function setStorage(StorageInterface $storage)
    {
        $this->_storage = $storage;
    }

    public function registerObserver(ObserverInterface $observer)
    {
        $this->_observers[] = $observer;
    }

    public function removeObserver(ObserverInterface $observer)
    {
        unset($this->_observers[array_search($observer, $this->_observers)]);
    }

    public function notifyObservers()
    {
        foreach($this->_observers as $observer)
        {
            $observer->update($this->_storage);
        }
    }
}