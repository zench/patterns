<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 05.03.2015
 * Time: 22:48
 */

namespace Observer\Storages;


class TraktirStorage implements StorageInterface
{
    private $_menu;
    private $_order;
    private $_employee;

    public function __construct($menu, $order, $employee)
    {
        $this->_menu = $menu;
        $this->_order = $order;
        $this->_employee = $employee;
    }

    /**
     * @return mixed
     */
    public function getMenu()
    {
        return $this->_menu;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * @return mixed
     */
    public function getEmployee()
    {
        return $this->_employee;
    }


}