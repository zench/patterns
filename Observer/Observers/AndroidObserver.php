<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 05.03.2015
 * Time: 22:52
 */

namespace Observer\Observers;


use Observer\Subjects\SubjectInterface;
use Observer\Storages\StorageInterface;

class AndroidObserver implements ObserverInterface
{
    private $_subject;

    private $_menu;

    private $_order;

    public function __construct(SubjectInterface $subject)
    {
        $this->_subject = $subject;
        $this->_subject->registerObserver($this);
    }

    public function update(StorageInterface $storage)
    {
        $this->_menu = $storage->getMenu();
        $this->_order = $storage->getOrder();
        $this->display();
    }

    public function display()
    {
        echo json_encode($this->_menu);
        echo "<br />";
        echo json_encode($this->_order);
        echo "<br />";
    }
}