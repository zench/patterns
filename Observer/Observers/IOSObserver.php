<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 05.03.2015
 * Time: 22:59
 */

namespace Observer\Observers;


use Observer\Subjects\SubjectInterface;
use Observer\Storages\StorageInterface;

class IOSObserver implements ObserverInterface
{
    private $_subject;

    private $_employee;

    private $_order;

    public function __construct(SubjectInterface $subject)
    {
        $this->_subject = $subject;
        $this->_subject->registerObserver($this);
    }

    public function update(StorageInterface $storage)
    {
        $this->_employee = $storage->getEmployee();
        $this->_order = $storage->getOrder();
        $this->display();
    }

    public function display()
    {
        echo serialize($this->_employee);
        echo "<br />";
        echo serialize($this->_order);
        echo "<br />";
    }
}