<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 05.03.2015
 * Time: 22:12
 */

namespace Observer\Observers;


use Observer\Storages\StorageInterface;

interface ObserverInterface
{
    public function update(StorageInterface $storage);

    public function display();
}