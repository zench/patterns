<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 17:51
 */

namespace Ducks\Quakable;


class CommonQuakable extends QuakableAbstract
{
    public function quak()
    {
        echo 'I\'m quaking';
    }
}