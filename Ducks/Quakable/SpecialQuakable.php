<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 17:49
 */

namespace Ducks\Quakable;


class SpecialQuakable extends QuakableAbstract
{
    public function quak()
    {
        echo 'I\'m quaking special';
    }
}