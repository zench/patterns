<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 12:59
 */

namespace Ducks\Quakable;


interface QuakableInterface
{
    public function quak();
}