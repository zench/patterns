<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 17:54
 */

namespace Ducks;


class SpecialDuck extends DuckAbstract
{
    public function display()
    {
        echo 'I\'m special duck!';
    }
}