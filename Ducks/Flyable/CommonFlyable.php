<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 17:52
 */

namespace Ducks\Flyable;


class CommonFlyable extends FlyableAbstract
{
    public function fly()
    {
        echo 'I\'m flying';
    }

}