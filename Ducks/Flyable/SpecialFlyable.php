<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 13:04
 */

namespace Ducks\Flyable;


class SpecialFlyable extends FlyableAbstract
{
    public function fly()
    {
        echo 'I have special flying';
    }
}