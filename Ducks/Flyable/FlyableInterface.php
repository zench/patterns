<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 12:56
 */

namespace Ducks\Flyable;


interface FlyableInterface
{
    public function fly();
}