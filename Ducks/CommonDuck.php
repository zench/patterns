<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 17:56
 */

namespace Ducks;


class CommonDuck extends DuckAbstract
{
    public function display()
    {
        echo 'I\'m common duck!';
    }
}