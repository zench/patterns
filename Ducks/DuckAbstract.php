<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.03.2015
 * Time: 12:45
 */

namespace Ducks;

use Ducks\Flyable\CommonFlyable;
use Ducks\Flyable\FlyableInterface;
use Ducks\Quakable\QuakableInterface;

abstract class DuckAbstract
{
    protected $flyable;
    protected $quakable;

    public function __construct(FlyableInterface $flyable, QuakableInterface $quakable)
    {
        $this->flyable = $flyable;
        $this->quakable = $quakable;
    }

    public function swim()
    {
        echo 'I\'m swimming';
    }

    public abstract function display();

    public function fly()
    {
        $this->flyable->fly();
    }

    public function quak()
    {
        $this->quakable->quak();
    }

}