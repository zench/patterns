<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.04.15
 * Time: 21:38
 */

namespace Facade;


class WaterManagementSubsystem
{
    public function fillWater($litres)
    {
        echo "filling ".$litres." litres of water<br />";
    }

    public function emptySystem()
    {
        echo "water is pouring<br />";
    }
} 