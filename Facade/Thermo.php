<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.04.15
 * Time: 21:38
 */

namespace Facade;


class Thermo
{
    public function heatUp($temperature)
    {
        echo "Heating to ".$temperature." degrees<br />";
    }
} 