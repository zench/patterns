<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.04.15
 * Time: 21:36
 */

namespace Facade;


class Dryer
{
    public function spin($time, $rotations)
    {
        echo "spinning ".$time." seconds with ".$rotations." rotations<br />";
    }
} 