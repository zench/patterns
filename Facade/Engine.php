<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.04.15
 * Time: 21:35
 */

namespace Facade;


class Engine
{
    public function rotate($time)
    {
        echo "Rotating ".$time." seconds<br />";
    }
} 