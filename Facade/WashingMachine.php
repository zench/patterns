<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.04.15
 * Time: 21:47
 */

namespace Facade;


class WashingMachine
{
    private $_dryer;
    private $_waterManagementSystem;
    private $_engine;
    private $_thermo;

    public function __construct(Engine $engine, Thermo $thermo, WaterManagementSubsystem $waterManagementSubsystem, Dryer $dryer)
    {
        $this->_engine = $engine;
        $this->_dryer = $dryer;
        $this->_waterManagementSystem = $waterManagementSubsystem;
        $this->_thermo = $thermo;
    }

    public function cottonWashing()
    {
        $this->_waterManagementSystem->fillWater(20);
        $this->_thermo->heatUp(90);
        $this->_engine->rotate(600);
        $this->_waterManagementSystem->emptySystem();
        $this->_waterManagementSystem->fillWater(20);
        $this->_thermo->heatUp(70);
        $this->_engine->rotate(1200);
        $this->_waterManagementSystem->emptySystem();
        $this->_waterManagementSystem->fillWater(20);
        $this->_thermo->heatUp(70);
        $this->_engine->rotate(1200);
        $this->_waterManagementSystem->emptySystem();
        $this->_waterManagementSystem->fillWater(20);
        $this->_engine->rotate(600);
        $this->_waterManagementSystem->emptySystem();
        $this->_waterManagementSystem->fillWater(20);
        $this->_engine->rotate(600);
        $this->_waterManagementSystem->emptySystem();
        $this->_dryer->spin(600, 1200);
    }

    public function woolWashing()
    {
        $this->_waterManagementSystem->fillWater(30);
        $this->_thermo->heatUp(40);
        $this->_engine->rotate(300);
        $this->_waterManagementSystem->emptySystem();
        $this->_waterManagementSystem->fillWater(30);
        $this->_thermo->heatUp(30);
        $this->_engine->rotate(400);
        $this->_waterManagementSystem->emptySystem();
        $this->_waterManagementSystem->fillWater(30);
        $this->_engine->rotate(300);
        $this->_waterManagementSystem->emptySystem();
        $this->_waterManagementSystem->fillWater(30);
        $this->_engine->rotate(300);
        $this->_waterManagementSystem->emptySystem();
        $this->_dryer->spin(300, 800);
    }
} 