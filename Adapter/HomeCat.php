<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 13.04.15
 * Time: 23:03
 */

namespace Adapter;


class HomeCat implements CatInterface
{
    private $_name;

    public function __construct($name)
    {
        $this->_name = $name;
    }

    public function say()
    {
        echo "fr-fr-fr";
    }

    public function getName()
    {
        return $this->_name;
    }

} 