<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 13.04.15
 * Time: 23:00
 */

namespace Adapter;


interface CatInterface
{
    public function say();

    public function getName();
} 