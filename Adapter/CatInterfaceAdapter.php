<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 13.04.15
 * Time: 23:17
 */

namespace Adapter;


class CatInterfaceAdapter implements CatInterface
{
    private $_wildCat;

    public function __construct($wildCat)
    {
        $this->_wildCat = $wildCat;
    }

    public function say()
    {
        $this->_wildCat->sing();
    }

    public function getName()
    {
        $className = get_class($this->_wildCat);
        $classPath = explode('\\', $className);
        return $classPath[count($classPath)-1];
    }
}