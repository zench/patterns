<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 13.04.15
 * Time: 23:06
 */

namespace Adapter;


class CatPrinter
{
    private $_cat;
    public function __construct(CatInterface $cat)
    {
        $this->_cat = $cat;
    }

    public function printCat()
    {
        echo "I'm ".$this->_cat->getName()."<br />";
        echo $this->_cat->say()."<br /><br />";
    }
} 