<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.04.15
 * Time: 22:03
 */

namespace TemplateMethod;


class HotDog extends FastFood
{

    public function prepareMainIngredient()
    {
        echo "Sausages are added<br />";
    }

    public function addTopping()
    {
        if($this->isTopping()) {
            echo "Mustard is added<br />";
        }
    }
}