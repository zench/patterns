<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.04.15
 * Time: 22:02
 */

namespace TemplateMethod;


abstract class FastFood
{
    protected $_topping = false;

    public function prepare()
    {
        $this->roastBread();
        $this->prepareMainIngredient();
        $this->putVegetables();
        $this->addTopping();
    }

    abstract public function prepareMainIngredient();

    public function roastBread()
    {
        echo "Bread is roasted<br />";
    }

    public function putVegetables()
    {
        echo "Vegetables are put<br />";
    }

    public function isTopping()
    {
        return $this->_topping;
    }

    public function customerWantsTopping()
    {
        $this->_topping = true;
    }

    abstract public function addTopping();
} 