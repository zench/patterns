<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.03.15
 * Time: 0:04
 */

namespace FabricMethod\Facilities;


abstract class VolkswagenFacility
{
    public function getCar($type)
    {
        $car = $this->createCar($type);
        $car->configure();
        $car->bodyWelding();
        $car->engineInstalling();
        $car->coloring();
        $car->wheelsBolting();
        echo "<br />";
    }

    protected abstract  function createCar($type);
} 