<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.03.15
 * Time: 0:07
 */

namespace FabricMethod\Facilities;


use FabricMethod\Cars\Car;
use FabricMethod\Cars\RussianGolf;
use FabricMethod\Cars\RussianPassat;
use FabricMethod\Cars\RussianTiguan;
use FabricMethod\Cars\RussianTouareg;

class RussianVolkswagenFacility extends VolkswagenFacility
{

    protected function createCar($type)
    {
        $car = new Car();
        if($type == 'Golf') {
            $car = new RussianGolf();
        } elseif($type == 'Passat') {
            $car = new RussianPassat();
        } elseif($type == 'Tiguan') {
            $car = new RussianTiguan();
        } elseif($type == 'Touareg') {
            $car = new RussianTouareg();
        }

        return $car;
    }
}