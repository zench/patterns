<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.03.15
 * Time: 0:07
 */

namespace FabricMethod\Facilities;


use FabricMethod\Cars\Car;
use FabricMethod\Cars\DeutschGolf;
use FabricMethod\Cars\DeutschPassat;
use FabricMethod\Cars\DeutschTiguan;
use FabricMethod\Cars\DeutschTouareg;

class DeutschVolkswagenFacility extends VolkswagenFacility
{

    protected function createCar($type)
    {
        $car = new Car();
        if($type == 'Golf') {
            $car = new DeutschGolf();
        } elseif($type == 'Passat') {
            $car = new DeutschPassat();
        } elseif($type == 'Tiguan') {
            $car = new DeutschTiguan();
        } elseif($type == 'Touareg') {
            $car = new DeutschTouareg();
        }

        return $car;
    }
}