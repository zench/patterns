<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 24.03.15
 * Time: 23:44
 */

namespace FabricMethod\Cars;


class Car
{
    protected $body = 'hetchback';
    protected $name = 'Golf';
    protected $engine = 'diesel';
    protected $wheels = 16;
    protected $color = 'white';

    public function configure()
    {
        echo 'configuring '.$this->name."<br />";
        echo 'color - '.$this->color."<br />";
        echo 'body - '.$this->body."<br />";
        echo 'engine - '.$this->engine."<br />";
        echo 'wheels - '.$this->wheels."<br /><br />";
    }

    public function bodyWelding()
    {
        echo "Body is welded<br />";
    }

    public function engineInstalling()
    {
        echo "Engine is installed<br />";
    }

    public function coloring()
    {
        echo "Car is colored<br />";
    }

    public function wheelsBolting()
    {
        echo "Wheels is bolted<br />";
    }


} 