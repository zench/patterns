<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 24.03.15
 * Time: 23:55
 */

namespace FabricMethod\Cars;


class RussianGolf extends Car
{
    public function __construct()
    {
        $this->name = 'Golf';
        $this->body = 'hatchback';
        $this->engine = 'gasoline';
    }
} 