<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 24.03.15
 * Time: 23:55
 */

namespace FabricMethod\Cars;


class RussianTouareg extends Car
{
    public function __construct()
    {
        $this->name = 'Touareg';
        $this->body = 'big crossover';
        $this->engine = 'gasoline';
    }
} 