<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 24.03.15
 * Time: 23:55
 */

namespace FabricMethod\Cars;


class DeutschPassat extends Car
{
    public function __construct()
    {
        $this->name = 'Passat';
        $this->body = 'sedan';
        $this->engine = 'diesel';
    }
} 