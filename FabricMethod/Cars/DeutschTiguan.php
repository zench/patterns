<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 24.03.15
 * Time: 23:55
 */

namespace FabricMethod\Cars;


class DeutschTiguan extends Car
{
    public function __construct()
    {
        $this->name = 'Tiguan';
        $this->body = 'crossover';
        $this->engine = 'diesel';
    }
} 