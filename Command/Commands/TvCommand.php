<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 03.04.15
 * Time: 11:44
 */

namespace Command\Commands;


use Command\ControlledSystems\Tv;

class TvCommand implements CommandInterface
{
    private $_tv;
    private $_previousState;

    public function __construct(Tv $tv)
    {
        $this->_tv = $tv;
    }

    public function execute()
    {
        $this->_previousState = $this->_tv->state;
        $this->_tv->turnOn();
    }

    public function undo()
    {
        $this->_previousState = $this->_tv->state;
        $this->_tv->turnOff();
    }

    public function __toString()
    {
        return "Включить телевизор";
    }
}