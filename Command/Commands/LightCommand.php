<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 03.04.15
 * Time: 11:35
 */

namespace Command\Commands;


use Command\ControlledSystems\Light;

class LightCommand implements CommandInterface
{
    private $_light;
    private $_states;

    public function __construct(Light $light)
    {
        $this->_light = $light;
        $this->_states = array();
    }
    public function execute()
    {
        array_push($this->_states, $this->_light->state);
        $this->_light->toggleLight();
    }

    public function undo()
    {
        $prev_state = array_pop($this->_states);
        $this->_light->setState($prev_state);
    }

    public function __toString()
    {
        return "Включить свет";
    }
}