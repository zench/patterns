<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 03.04.15
 * Time: 11:51
 */

namespace Command\Commands;


use Command\ControlledSystems\Teapot;

class TeapotCommand implements CommandInterface
{
    private $_teapot;
    private $_previousState;

    public function __construct(Teapot $teapot)
    {
        $this->_teapot = $teapot;
    }

    public function execute()
    {
        $this->_previousState = $this->_teapot->state;
        $this->_teapot->turnOn();
    }

    public function undo()
    {
        $this->_previousState = $this->_teapot->state;
        $this->_teapot->turnOff();
    }

    public function __toString()
    {
        return "Включить чайник";
    }
} 