<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 03.04.15
 * Time: 11:50
 */

namespace Command\Commands;


use Command\ControlledSystems\Music;

class MusicCommand implements CommandInterface
{
    private $_music;
    private $_previousState;

    public function __construct(Music $music)
    {
        $this->_music = $music;
    }

    public function execute()
    {
        $this->_previousState = $this->_music->state;
        $this->_music->turnOn();
    }

    public function undo()
    {
        $this->_previousState = $this->_music->state;
        $this->_music->turnOff();
    }

    public function __toString()
    {
        return "Включить музыку";
    }
} 