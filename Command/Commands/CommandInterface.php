<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 03.04.15
 * Time: 11:32
 */

namespace Command\Commands;


interface CommandInterface
{
    public function execute();

    public function undo();
} 