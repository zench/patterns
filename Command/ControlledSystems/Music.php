<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 03.04.15
 * Time: 10:29
 */

namespace Command\ControlledSystems;


class Music
{
    public $stateSigns = array('off' => 0, 'on' => 1);
    public $state;
    public function turnOn()
    {
        echo "Музыка включена<br />";
        $this->state = $this->stateSigns['on'];
    }

    public function turnOff()
    {
        echo "Музыка выключена<br />";
        $this->state = $this->stateSigns['off'];
    }
} 