<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 03.04.15
 * Time: 10:28
 */

namespace Command\ControlledSystems;


class Light
{
    public $lightStateSigns = array('off' => 0, 'low' => 1, 'medium' => 2, 'high' => 3);
    public $state;
    public function toggleLight()
    {
        switch($this->state) {
            case($this->lightStateSigns['off']):
                $this->state = $this->lightStateSigns['low'];
            break;

            case($this->lightStateSigns['low']):
                $this->state = $this->lightStateSigns['medium'];
            break;
            case($this->lightStateSigns['medium']):
                $this->state = $this->lightStateSigns['high'];
            break;
            case($this->lightStateSigns['high']):
                $this->state = $this->lightStateSigns['off'];
            break;
        }
        $this->printLight();
    }

    public function setState($state)
    {
        $this->state = $state;
        $this->printLight();
    }

    public function printLight()
    {
        switch($this->state) {
            case($this->lightStateSigns['off']):
                echo "Свет выключен<br />";
                break;

            case($this->lightStateSigns['low']):
                echo "Свет тусклый<br />";
                break;
            case($this->lightStateSigns['medium']):
                echo "Свет средний<br />";
                break;
            case($this->lightStateSigns['high']):
                echo "Свет яркий<br />";
                break;
        }
    }

    public function __toString()
    {
        return "Управление светом";
    }
} 