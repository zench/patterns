<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 03.04.15
 * Time: 10:29
 */

namespace Command\ControlledSystems;


class Tv
{
    public $stateSigns = array('off' => 0, 'on' => 1);
    public $state;
    public function turnOn()
    {
        echo "Телевизор включен<br />";
        $this->state = $this->stateSigns['on'];
    }

    public function turnOff()
    {
        echo "Телевизор выключен<br />";
        $this->state = $this->stateSigns['off'];
    }
} 