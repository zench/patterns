<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 03.04.15
 * Time: 11:52
 */

namespace Command;


use Command\Commands\CommandInterface;

class RemoteControl
{
    private $_buttons = array();

    public function pushButton($button)
    {
        if(isset($this->_buttons[$button])) {
            $this->_buttons[$button]->execute();
        }
    }

    public function setButtonForCommand($button, CommandInterface $command)
    {
        $this->_buttons[$button] = $command;
    }

    public function undoButton($button)
    {
        if(isset($this->_buttons[$button])) {
            $this->_buttons[$button]->undo();
        }
    }

    public function __toString()
    {
        $result = "";
        foreach($this->_buttons as $commandSign => $command) {
            $result .= $commandSign.": ".(string)$command."<br />";
        }
        return $result;
    }
} 