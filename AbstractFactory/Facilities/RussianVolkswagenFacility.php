<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 25.03.15
 * Time: 0:07
 */

namespace AbstractFactory\Facilities;


use AbstractFactory\Cars\Golf;
use AbstractFactory\Cars\Passat;
use AbstractFactory\Cars\Tiguan;
use AbstractFactory\Cars\Touareg;
use AbstractFactory\PartsFactory\RussianCarPartsFactory;

class RussianVolkswagenFacility extends VolkswagenFacility
{

    protected function createCar($type)
    {
        $factory = new RussianCarPartsFactory();
        if($type == 'Golf') {
            return new Golf($factory);
        } elseif($type == 'Passat') {
            return new Passat($factory);
        } elseif($type == 'Tiguan') {
            return new Tiguan($factory);
        } elseif($type == 'Touareg') {
            return new Touareg($factory);
        }

        return null;
    }
}