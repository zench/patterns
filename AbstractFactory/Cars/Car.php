<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 24.03.15
 * Time: 23:44
 */

namespace AbstractFactory\Cars;


use AbstractFactory\PartsFactory\CarPartsFactory;

abstract class Car
{
    protected $body = 'hatchback';
    protected $name = 'Golf';

    protected $engine;
    protected $wheels;
    protected $color;

    protected $_carPartsFactory;

    public function __construct(CarPartsFactory $factory)
    {
        $this->_carPartsFactory = $factory;
    }


    public function configure()
    {
        echo 'configuring '.$this->name."<br />";
        echo 'body - '.$this->body."<br />";
        $this->engine = $this->_carPartsFactory->createEngine();
        $this->color = $this->_carPartsFactory->createColor();
        $this->wheels = $this->_carPartsFactory->createWheels();
    }

    public function bodyWelding()
    {
        echo "Body is welded<br />";
    }

    public function engineInstalling()
    {
        echo "Engine is installed<br />";
    }

    public function coloring()
    {
        echo "Car is colored<br />";
    }

    public function wheelsBolting()
    {
        echo "Wheels is bolted<br />";
    }


} 