<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 24.03.15
 * Time: 23:55
 */

namespace AbstractFactory\Cars;


class Golf extends Car
{
    public function __construct($factory)
    {
        parent::__construct($factory);
        $this->name = 'Golf';
        $this->body = 'hatchback';
    }
} 