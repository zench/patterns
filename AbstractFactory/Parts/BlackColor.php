<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 26.03.15
 * Time: 23:17
 */

namespace AbstractFactory\Parts;


class BlackColor extends Paint
{
    public function __construct()
    {
        echo "black color <br />";
    }
} 