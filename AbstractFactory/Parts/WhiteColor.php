<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 26.03.15
 * Time: 23:17
 */

namespace AbstractFactory\Parts;


class WhiteColor extends Paint
{
    public function __construct()
    {
        echo "white color <br />";
    }
} 