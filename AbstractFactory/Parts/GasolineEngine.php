<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 26.03.15
 * Time: 23:17
 */

namespace AbstractFactory\Parts;


class GasolineEngine extends Engine
{
    public function __construct()
    {
        echo "gasoline engine <br />";
    }
} 