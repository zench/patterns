<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 26.03.15
 * Time: 23:03
 */

namespace AbstractFactory\PartsFactory;


abstract class CarPartsFactory
{
    abstract public function createEngine();
    abstract public function createWheels();
    abstract public function createColor();
} 