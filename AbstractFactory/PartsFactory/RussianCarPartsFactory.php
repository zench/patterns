<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 26.03.15
 * Time: 23:15
 */

namespace AbstractFactory\PartsFactory;


use AbstractFactory\Parts\BlackColor;
use AbstractFactory\Parts\GasolineEngine;
use AbstractFactory\Parts\MediumWheels;

class RussianCarPartsFactory extends CarPartsFactory
{

    public function createEngine()
    {
        return new GasolineEngine();
    }

    public function createWheels()
    {
        return new MediumWheels();
    }

    public function createColor()
    {
        return new BlackColor();
    }
}