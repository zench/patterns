<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 26.03.15
 * Time: 23:15
 */

namespace AbstractFactory\PartsFactory;


use AbstractFactory\Parts\WhiteColor;
use AbstractFactory\Parts\DieselEngine;
use AbstractFactory\Parts\BigWheels;

class DeutschCarPartsFactory extends CarPartsFactory
{

    public function createEngine()
    {
        return new DieselEngine();
    }

    public function createWheels()
    {
        return new BigWheels();
    }

    public function createColor()
    {
        return new WhiteColor();
    }
}